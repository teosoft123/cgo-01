GO_SDK_VERSION=go1.13.7
GO_SDK_BASE=$(HOME)/go-sdk

export GOBIN=$(CURDIR)
export GOPATH=$(CURDIR)/go

export MY_GOROOT := $(if $(JENKINS_GOROOT),$(JENKINS_GOROOT),$(GO_SDK_BASE)/$(GO_SDK_VERSION))

export GO=$(MY_GOROOT)/bin/go

export PACKAGE=tsvinev.com
export DELIVERABLE_NAME=cgo_there_and_back
export GO_LIB=lib$(DELIVERABLE_NAME).so
export GO_HEADER=lib$(DELIVERABLE_NAME).h
export GO_EXE=$(DELIVERABLE_NAME)

export NM=nm

info:
	$(info === This might help with troubleshooting ===)
	$(info HOME: $(HOME))
	$(info GOBIN: $(GOBIN))
	$(info GOPATH: $(GOPATH))
	$(info MY_GOROOT: $(MY_GOROOT))
	$(info GOROOT: $(GOROOT))

	@$(GO) env

clean:
	$(GO) clean
	$(RM) $(GO_LIB) $(GO_HEADER) $(GO_EXE)

deps:
	$(GO) get -d $(PACKAGE)

build:
	$(GO) build $(PACKAGE)

lib:
	$(GO) build -i -o $(GO_LIB) -buildmode=c-shared $(PACKAGE)

install:
	$(GO) build -i -o bgvault_go $(PACKAGE)

test:
	$(GO) test -v -count=1 $(PACKAGE)/bgerror $(PACKAGE)/bgconfig $(PACKAGE)/bgvault

run:
	$(GO) run $(PACKAGE)

internals:
	$(NM) -gU $(GO_LIB)
