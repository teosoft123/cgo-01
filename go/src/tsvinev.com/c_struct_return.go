package main

/*
typedef struct {
    int X;
    int Y;
} Vertex;

typedef int CustomerIdType;

typedef struct {
	int len;
    int *list;
} IntList;
*/
import "C"
import (
	"fmt"
)

//export getVertex
func getVertex(X, Y C.int) C.Vertex {
	return C.Vertex{X, Y}
}

func bool2Cint(b bool) C.int {
	if b {
		return 0
	}
	return -1
}

//export getVaultIds
func getVaultIds(customerId C.CustomerIdType, rv *C.IntList) (err C.int) {
	ids := []C.int{1, 2, 3, 4, 5}
	rv.len = C.int(len(ids))
	rv.list = &ids[0]
	return 0
}

//export checkVertex
func checkVertex(v *C.Vertex) C.int {
	return bool2Cint(v != nil)
}

//export scaleVertex
func scaleVertex(v *C.Vertex, xscale C.int, yscale C.int) {
	if v != nil {
		v.X *= xscale
		v.Y *= yscale
	}
}

//func (vertex C.Vertex) String() string {
//	return fmt.Sprintf("X: %d, Y: %d\n", vertex.X, vertex.Y)
//}

func (vertex C.Vertex) String() string {
	return fmt.Sprintf("X: %d, Y: %d\n", vertex.X, vertex.Y)
}

func main() {
	v := C.Vertex{1, 2}
	fmt.Println(&v) // this calls Stringer
	fmt.Println(checkVertex(&v))
	scaleVertex(&v, 2, 3)
	fmt.Println(&v) // this calls Stringer
	fmt.Println(v)  // but this does not

	cv := getVertex(4, 5)
	fmt.Println(&cv) // this calls Stringer
	scaleVertex(&cv, 10, 0)
	fmt.Println(&cv) // this calls Stringer

	il := C.IntList{}
	getVaultIds(1, &il)
	fmt.Printf("got length %v, address: %v\n", il.len, il.list)
	for i := 0; C.int(i) < il.len; i++ {
		fmt.Printf("list[%d] = %v\n", i, *il.list+C.int(i)) // Voila! pointer arithmetics in Go... well, C, actually
	}

	//array := reflect.SliceHeader{uintptr(unsafe.Pointer(il.list)), int(il.len), int(il.len)}
	//fmt.Println(array)

}
