package main

import (
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"io/ioutil"
	"log"
)

func connectToMySqlWithTls(basePath string) (*sql.DB, error) {
	rootCertPool := x509.NewCertPool()
	pem, err := ioutil.ReadFile(basePath + "/" + "ca.pem")
	if err != nil {
		log.Fatal(err)
	}
	if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
		log.Fatal("Failed to append PEM.")
	}
	clientCert := make([]tls.Certificate, 0, 1)
	certs, err := tls.LoadX509KeyPair(basePath+"/"+"client-cert.pem", basePath+"/"+"client-key.pem")
	if err != nil {
		log.Fatal(err)
	}
	clientCert = append(clientCert, certs)
	mysql.RegisterTLSConfig("custom", &tls.Config{
		RootCAs:      rootCertPool,
		Certificates: clientCert,
	})
	db, err := sql.Open("mysql", "root:admin@tcp(localhost:33060)/test?tls=custom")
	return db, err
}

func main() {
	db, err := connectToMySqlWithTls("/Users/oleg/projects/cgo-01/go/src/tsvinev.com/mysql")
	fmt.Printf("db: %v, err: %v\n", db, err)
	rv := db.QueryRow("select 2+3")
	var result int = 0
	err = rv.Scan(&result)
	if err != nil {
		fmt.Printf("Error executing query: %v", err)
	} else {
		fmt.Printf("select 2+3 = %d", result)
	}
}
