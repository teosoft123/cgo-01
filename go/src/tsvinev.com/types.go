package main

type Base interface {
}

type Interim interface {
	Caca()
}

type Derived interface {
	Hello() string
}

type Greeting struct {
	greeting string
}

func (g *Greeting) Hello() string {
	return g.greeting
}
