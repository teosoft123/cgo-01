package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestTypeCheckTest(t *testing.T) {

	m := Greeting{
		greeting: "hello world.",
	}

	var b Base = &m

	testCases := []reflect.Type{
		reflect.TypeOf((*Base)(nil)).Elem(),
		reflect.TypeOf((*Interim)(nil)).Elem(),
		reflect.TypeOf((*Derived)(nil)).Elem(),
	}

	t.Run("value assignable to interface", func(test *testing.T) {
		bType := reflect.TypeOf(b)
		for _, tc := range testCases {
			t.Run("test case tc", func(t *testing.T) {
				dItype := tc
				fmt.Printf("does %v implement %v?\n", bType, dItype)
				if !bType.Implements(dItype) {
					t.Errorf("b does not implement type %v", dItype)
				}
			})
		}
	})

}
